package utility;

public class Constants {

	/*Constant member variables from SearchManager*/
	
	public final static String ACTION_INDEX = "index";
	public final static String ACTION_SEARCH = "search";
	public final static int CANDIDATE_SEARCHER = 1;
	public final static int CANDIDATE_PROCESSOR = 2;
	public final static int CLONE_VALIDATOR = 3;
	public final static int CLONE_REPORTER = 4;
	
	public final static float threshold = 7;
	public final static int QBQ_THREADS = 1;
	public final static int QCQ_THREADS = 1;
	public final static int VCQ_THREADS = 1;
	public final static int RCQ_THREADS = 1;
	public final static int QBQ_SIZE=5;
	public final static int QCQ_SIZE=5;
	public final static int VCQ_SIZE=5;
	public final static int RCQ_SIZE=5;
	public final static Integer MUL_FACTOR = 100; //also in CloneDetectorWithFilter and CloneDetector
	
	/*Constant member variables from Util*/
	
	public final static String CSV_DELIMITER = "~";
	public final static String INDEX_DIR = "index";
	public final static String GTPM_DIR = "gtpm";
	public final static String FWD_INDEX_DIR = "fwdindex";
	public final static String INDEX_DIR_NO_FILTER = "index_nofilter";

    /*Constant member variables from TermSorter*/
    
    public final static String SORTED_FILES_DIR = "output/sortedFiles";
    
    /*Constant member variables from CloneDetector and CloneDetectorWithFilter*/

    public final static String SIMILARITY_OVERLAP = "overlap";
    public final static String SIMILARITY_JACCARD = "jaccard";
    public final static String OUTPUT_DIR_PREFIX = "outputnew";

	/*Constant member variables from CloneDetectorWithFilter*/
	
    public final static String LOC_FILTER_CANDIDATES = "pos_filter_c";
    public final static String LOC_FILTER_VALIDATION = "pos_filter_v";
    public final static String LOC_FILTER_BOTH = "pos_filter_cv";
    public final static String LOC_FILTER_NONE = "pos_filter_none";
}

